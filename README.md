# Source

Тестовое задание Source

Preview: https://source-test.netlify.app/

## Структура проекта

*  ```/krasmontage```
    *  ```/build``` (конфиги webpack)
    *  ```/dist``` (build-версия проекта)
    *  ```/src``` 
        *  ```/assets``` (различные ресурсы)
            *  ```/fonts``` (шрифты)
            *  ```/img``` (изображения)
            *  ```/js``` (js код)
            *  ```/scss``` (scss код)
                *  ```/components``` (scss для компонентов)
                    *  ```/sections``` (scss для секций)
                    *  ```/swipers``` (scss для слайдеров)
                    *  ```/*.scss``` (scss для остальных компонентов)
                *  ```/utils``` (переменные, миксины и т.д.)
                    *  ```/fonts.scss``` (font-face для шрифтов)
                    *  ```/mixins.scss``` (миксины)
                    *  ```/reset.scss``` (reset стилей)
                    *  ```/vars.scss``` (переменные)
                *  ```/main.scss``` (основной файл стилей)
        * ```/index.html``` (основной html)
        * ```/index.js``` (входная точка для webpack)

## Использование

Для просмотра build-версии:
1. Перейти на https://source-test.netlify.app/
2. PROFIT

Для запуска проекта на dev-сервере необходимо: 
1.  Клонировать репозиторий (```git clone git@gitlab.com:ghettojezuz/source-test-task.git```)
2.  Установить ```devDependencies``` и ```dependencies``` из ```package.json``` с помощью ```npm```
3.  Ввести команду ```npm run dev``` в консоли, находясь в директории ```/source-test-task```

>  git add -- . ':!source-test-task/node_modules'