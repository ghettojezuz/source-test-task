// JS - ./js/index.js
import 'swiper/js/swiper.min';
import './assets/js/main';

// SCSS/CSS
import './assets/scss/main.scss';
import 'swiper/css/swiper.min.css';