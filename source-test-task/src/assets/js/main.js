import {toggleMenu} from "./toggleMenu";
import {setSwipers} from "./swipers";

const main = () => {

    // ----------- MAIN LOOP -----------

    setSwipers();

    // ----------- EVENT LISTENERS -----------

    const mobileMenuOpenToggler = document.getElementById('menu-open-toggler');
    mobileMenuOpenToggler.addEventListener('click', () => {
        toggleMenu();
    });

    const mobileMenuCloseToggler = document.getElementById('menu-close-toggler');
    mobileMenuCloseToggler.addEventListener('click', () => {
        toggleMenu();
    });

    const mobileMenuItems = [...document.getElementsByClassName('header-navigation-mobile__item')];
    mobileMenuItems.forEach((item) => {
        item.addEventListener('click', () => {
            toggleMenu();
        })
    })
};

main();