import Swiper from "swiper";

export const setSwipers = () => {

    const FirstSectionSwiper = new Swiper('.first-section-swiper', {
        direction: 'horizontal',
        slidesPerView: 1,
        speed: 800,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
    });

    const WhatWeDoSwiper = new Swiper('.what-we-do-swiper', {
        direction: 'horizontal',
        slidesPerView: 1,
        speed: 800,
        autoplay: {
            delay: 5000,
        },
        pagination: {
            el: '.swiper-pagination',
            type: 'bullets',
        },
    });
};