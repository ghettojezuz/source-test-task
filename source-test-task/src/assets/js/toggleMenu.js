export const toggleMenu = () => {
    const mobileMenu = document.getElementById('header-navigation-mobile');
    const activeClass = 'active';
    if (mobileMenu.className.includes(activeClass)) {
        mobileMenu.classList.remove(activeClass);
    } else {
        mobileMenu.classList.add(activeClass);
    }
};